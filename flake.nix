{
  description = "hello-template-haskell";

  inputs = {
    haskellNix.url = "github:input-output-hk/haskell.nix";
    nixpkgs.follows = "haskellNix/nixpkgs-unstable";
    flake-utils.follows = "haskellNix/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, haskellNix, ... }:
    flake-utils.lib.eachSystem [ "x86_64-linux" ] (system:
      let
        overlay = final: prev: {
          hsPkgs = final.haskell-nix.project' rec {
            src = ./.;
            compiler-nix-name = "ghc924";
            shell = {
              tools = {
                # brittany = { };
                cabal = { };
                cabal-fmt = { };
                fourmolu = { };
                ghcid = { };
                haskell-language-server = { };
                hlint = { };
                hoogle = { };
                # stylish-haskell = { };
                tasty-discover = { };
              };

              # Non-Haskell shell tools.
              buildInputs = with pkgs; [ ];
            };
          };
        };
        pkgs = import nixpkgs {
          inherit system;
          overlays = [ haskellNix.overlay overlay ];
        };
        flake = pkgs.hsPkgs.flake { };
      in flake // {
        defaultPackage =
          flake.packages."hello-template-haskell:exe:hello-template-haskell-exe";
      });
}
