{-# LANGUAGE TemplateHaskell #-}

module MyTH (
  myExp,
) where

import Language.Haskell.TH

myExp :: Q Exp
myExp = [e|123 :: Int|]
