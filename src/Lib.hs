{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -Wno-orphans #-}

module Lib (
  someFunc,
) where

import Data.Proxy (Proxy (..))

import Dummy (Dummy (..), deriveDummy)
import MyTH (myExp)

deriveDummy ''Bool False
deriveDummy ''Int (145 :: Int)

someFunc :: IO ()
someFunc = do
  putStrLn $ "TH expression: " <> show $myExp

  putStrLn $ "dummy True: " <> show (dummy (Proxy :: Proxy Bool))
  putStrLn $ "special True: " <> show (special (Proxy :: Proxy Bool))
  putStrLn $ "dummy (42::Int): " <> show (dummy (Proxy :: Proxy Int))
  putStrLn $ "special (42::Int): " <> show (special (Proxy :: Proxy Int))
