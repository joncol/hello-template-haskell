{-# LANGUAGE TemplateHaskell #-}

module Dummy (
  Dummy (..),
  deriveDummy,
) where

import Data.Proxy (Proxy (..))
import Data.Text (Text)
import Language.Haskell.TH
import TextShow (TextShow, showt)

class Dummy a where
  dummy :: Proxy a -> Text
  special :: Proxy a -> Text

deriveDummy :: TextShow a => Name -> a -> Q [Dec]
deriveDummy name x =
  [d|
    instance Dummy $a where
      dummy Proxy = "-> dummy for " <> n
      special _ = "-> special, x = " <> t
    |]
  where
    a = conT name
    n = showt (nameBase name)
    t = showt x
